const express = require("express");

// Mongoose is a package that allows us to create Schemas to Model our data structures and to manipulate our database using different access methods.
const mongoose = require("mongoose");

const port = 3001;
const app = express();

	//[Section] MongoDB connection
		//Syntax:
			// mongoose.connect("mongoDBconnectionString", { options to avoide errors in our connection})

	mongoose.connect("mongodb+srv://admin:admin@batch245-rebanal.hxprcml.mongodb.net/?retryWrites=true&w=majority", 
			{
				//Allows us to avoid any current and future errors while connecting to MONGODB
				useNewUrlParser: true,
				useUnifiedTopology: true

		})

	let db = mongoose.connection;

	//error handling in connecting
	db.on("error", console.error.bind(console, "Connection error"));

	//this will be triggered if the connection is succesful.
	db.once("open", ()=> console.log("We're connected to the cloud database!"))

		//Mongoose Schemas
			//Schemas determine the structure of the documents to be written in the database
			//Schemas act as the blueprint to our data
			//Syntax
				//const schemaName = new mongoose.Schema({keyvaluepairs})
		//taskSchema it contains two properties: name & status
		//required is used to specify that a field must not be empty

		//default - is used if a field value is not supplied

		const taskSchema = new mongoose.Schema({
				name: {
					type: String,
					required: [true, "Task name is required!"]
				},
				status: {
					type: String,
					default: "pending"
				}
		});

//ACTIVITY - user schema
		const userSchema = new mongoose.Schema({
			username:{
				type: String,
				required:[true, "Username is required!"]
			},
			
				password:{
					type:String,
					required:[true, "password is required!"]
				}
			
		});

		//[Section] Models
			//Uses schema to create/instatiate documents/objects that follows our schema structure

			//the variable/object that will be created can be used to run commands with our database

			//Syntax:
				//const variableName = mongoose.model("collectionName", schemaName);

		const Task = mongoose.model("Task", taskSchema)

//ACTIVITY - user model
		const User = mongoose.model("User", userSchema)

	//middlewares
	app.use(express.json()) //Allowsthe app to read json data
	app.use(express.urlencoded({extended: true})) //it will allow our app to read data from forms.

	//[Sections] Routing
		//Create/add new task
			//1. Check if the task is existing.
					//if task alrady exist in the database, we will return a message "The task is already existing!"
					//if the task doesn't exits in the database, we will add it in the database

//ACTIVITY - user routine
	app.post("/signup", (request, response) => {
		let input = request.body

		User.findOne({username: input.username}, (error, result) => {
				console.log(result);

				if( result !== null){
					return response.send("The username is already existing!");
				}else{
					let newUser = new User({
						username: input.username,
						password: input.password
					})

					newUser.save((saveError, savedUser) =>{
						if(saveError){
							return console.log(saveError);
						}
						else{
							return response.send("New User added!")
						}
					})
				}
			})
	})
	
	app.get("/signup", (request, response) => {
		User.find({}, (error, result) => {
			if(error){
				console.log(error);
			}else{
				return response.send(result);
			}
		})
	})





	app.post("/tasks", (request, response) => {
		let input = request.body
		console.log(input.status);

		if(input.status === undefined){
			Task.findOne({name: input.name}, (error, result) => {
				console.log(result);

				if( result !== null){
					return response.send("The task is already existing!");
				}else{
					let newTask = new Task({
						name: input.name
					})

					newTask.save((saveError, savedTask) =>{
						if(saveError){
							return console.log(saveError);
						}
						else{
							return response.send("New Task created!")
						}
					})
				}
			})
		}else{
			Task.findOne({name: input.name}, (error, result) => {
				console.log(result);

				if( result !== null){
					return response.send("The task is already existing!");
				}else{
					let newTask = new Task({
						name: input.name,
						status: input.status
					})

					
					newTask.save((saveError, savedTask) =>{
						if(saveError){
							return console.log(saveError);
						}
						else{
							return response.send("New Task created!")
						}
					})
				}
			})
		}
			
	})

	
	app.get("/tasks", (request, response) => {
		Task.find({}, (error, result) => {
			if(error){
				console.log(error);
			}else{
				return response.send(result);
			}
		})
	})


app.listen(port, ()=> console.log(`Server is running at port ${port}!`))

